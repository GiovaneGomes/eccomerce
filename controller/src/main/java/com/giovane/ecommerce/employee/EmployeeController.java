package com.giovane.ecommerce.employee;

import com.giovane.ecommerce.request.EmployeeRequest;
import com.giovane.ecommerce.response.EmployeeResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/ecommerce/employee")
public class EmployeeController {

    private final EmployeeFacade facade;

    @ResponseStatus(OK)
    @PostMapping
    public EmployeeResponse save(@RequestBody @Valid EmployeeRequest employeeRequest) {
        return facade.save(employeeRequest);
    }

    @ResponseStatus(NO_CONTENT)
    @PutMapping("/{id}")
    public EmployeeResponse update(@RequestBody @Valid EmployeeRequest employeeRequest, @PathVariable String id) {
        return facade.update(employeeRequest, id);
    }

    @ResponseStatus(NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable String id) {
        facade.deleteById(id);
    }

    @ResponseStatus(OK)
    @GetMapping("/{id}")
    public EmployeeResponse findById(@PathVariable String id) {
        return facade.findById(id);
    }

    @ResponseStatus(OK)
    @GetMapping
    public List<EmployeeResponse> findAll() {
        return facade.findAll();
    }


}
