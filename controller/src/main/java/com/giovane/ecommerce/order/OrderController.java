package com.giovane.ecommerce.order;

import com.giovane.ecommerce.request.OrderRequest;
import com.giovane.ecommerce.response.OrderResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/ecommerce/order")
public class OrderController {

    private final OrderFacade facade;

    @ResponseStatus(OK)
    @PostMapping
    public OrderResponse save(@RequestBody @Valid OrderRequest orderRequest) {
        return this.facade.save(orderRequest);
    }

    @ResponseStatus(NO_CONTENT)
    @PutMapping("/{id}")
    public OrderResponse update(@RequestBody @Valid OrderRequest orderRequest, @PathVariable String id) {
        return facade.update(orderRequest, id);
    }

    @ResponseStatus(NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable String id) {
        facade.deleteById(id);
    }

    @ResponseStatus(OK)
    @GetMapping("/{id}")
    public OrderResponse findById(@PathVariable String id) {
        return facade.findById(id);
    }

    @ResponseStatus(OK)
    @GetMapping
    public List<OrderResponse> findAll() {
        return facade.findAll();
    }

}
