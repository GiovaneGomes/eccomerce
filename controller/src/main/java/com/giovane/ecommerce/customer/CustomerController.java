package com.giovane.ecommerce.customer;

import lombok.*;
import java.util.List;
import javax.validation.*;
import org.springframework.web.bind.annotation.*;
import com.giovane.ecommerce.request.CustomerRequest;
import com.giovane.ecommerce.response.CustomerResponse;
import static org.springframework.http.HttpStatus.*;

// É utilizado para criar o construtor da CustomerFacade para usar a Injeção de Dependência.
@AllArgsConstructor
// Numa aplicação Web se usa @Controller, ele é quem retorna a view, ou seja, o HTML + CSS. Com as API RESTful,
// se quer retornar os dados em JSON ou XML, antes do Spring 4 teria que anotar a classe ou os métodos com a anotação
// @ResponseBody. Porém, a anotação @RestController foi adicionada no Spring 4 para facilitar o desenvolvimento de
// serviços Web RESTful, contendo as duas anotações @Controller e @ResponseBody, onde não é mais necessário a colocação
// do @ResponseBody em todos os métodos.
@RestController
// É uma anotação usada em nível de classe e método. Quando usada em nível de classe, especifíca o tipo de requisição
// que esta classe controladora lida, ou seja, mapeia as requisições Web em classe e métodos.
@RequestMapping("/api/v1/ecommerce/customer")
public class CustomerController {

    // A visibilidade private quer dizer que apenas esta classe poderá acessar essa variável.
    // E o final é para que o valor não possa mais ser modificado, ou seja, imutável.
    private final CustomerFacade facade;

    // Essa anotação especifíca o tipo de status code que queremos retornar.
    @ResponseStatus(CREATED)
    // É uma versão especializada do @RequestMapping, shortcut para @RequestMapping(method = RequestMethod. POST).
    @PostMapping
    // O @RequestBody, assim como o @ResponseBody, informam ao Spring o que ele deve serializar ou desserializar.
    // A serialização fica por parte da biblioteca Jackson, que já está embutida no Spring Framework. É ele quem
    // converte de Java para JSON.
    // O @Valid funciona ....
    public CustomerResponse save(@RequestBody @Valid CustomerRequest customerRequest) {
        return facade.save(customerRequest);
    }

    @ResponseStatus(NO_CONTENT)
    @PutMapping(value = "/{id}")
    public CustomerResponse update(@RequestBody @Valid CustomerRequest customerRequest, @PathVariable String id) {
        return facade.update(customerRequest, id);
    }

    @ResponseStatus(NO_CONTENT)
    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id) {
        facade.deleteById(id);
    }

    @ResponseStatus(OK)
    @GetMapping("/{id}")
    public CustomerResponse findById(@PathVariable String id) {
        return facade.findById(id);
    }

    @ResponseStatus(OK)
    @GetMapping
    public List<CustomerResponse> findAll() {
        return facade.findAll();
    }

    //  fazer método delete com o @RequestParam

}
