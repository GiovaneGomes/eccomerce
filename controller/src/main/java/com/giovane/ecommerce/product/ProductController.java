package com.giovane.ecommerce.product;

import com.giovane.ecommerce.request.ProductRequest;
import com.giovane.ecommerce.response.ProductResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/ecommerce/product")
public class ProductController {

    private final ProductFacade facade;

    @ResponseStatus(OK)
    @PostMapping
    public ProductResponse save(@RequestBody @Valid ProductRequest productRequest) {
        return facade.save(productRequest);
    }

    @ResponseStatus(NO_CONTENT)
    @PutMapping("/{id}")
    public ProductResponse update(@RequestBody @Valid ProductRequest productRequest, @PathVariable String id) {
        return facade.update(productRequest, id);
    }

    @ResponseStatus(NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable String id) {
        facade.deleteById(id);
    }

    @ResponseStatus(OK)
    @GetMapping("/{id}")
    public ProductResponse findById(@PathVariable String id) {
        return facade.findById(id);
    }

    @ResponseStatus(OK)
    @GetMapping
    public List<ProductResponse> findAll() {
        return facade.findAll();
    }

}
