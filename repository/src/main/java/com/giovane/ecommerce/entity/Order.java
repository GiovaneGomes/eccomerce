package com.giovane.ecommerce.entity;

import com.giovane.ecommerce.commons.StatusOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Document("orders")
public class Order {
    @Id
    private String id;
    private LocalDate dataCreated;
    private List<Product> products;
    private StatusOrder status;
}
