package com.giovane.ecommerce.entity;

import com.giovane.ecommerce.commons.Department;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Document("employees")
public class Employee {
    @Id
    private String id;
    private String name;
    private String image;
    private BigDecimal salary;
    private String login;
    private String password;
    private String phoneNumber;
    private LocalDate beginContract;
    private Department department;
    private ZipCode zipCode;
}
