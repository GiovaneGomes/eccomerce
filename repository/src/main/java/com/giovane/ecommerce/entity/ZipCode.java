package com.giovane.ecommerce.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ZipCode {
    @JsonProperty("cep")
    private String zipCode;
    @JsonProperty("logradouro")
    private String publicArea;
    @JsonProperty("complemento")
    private String complement;
    @JsonProperty("bairro")
    private String district;
    @JsonProperty("localidade")
    private String locality;
}
