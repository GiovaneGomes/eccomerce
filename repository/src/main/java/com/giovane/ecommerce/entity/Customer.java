package com.giovane.ecommerce.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Document("clients")
public class Customer {
    @Id
    private String id;
    private String name;
    private String image;
    private String login;
    private String password;
    private ZipCode zipCode;
}
