package com.giovane.ecommerce.commons;

public enum Department {
    WEB_DESIGNER, BUSINESS_ANALYST, MARKETING, DEVELOPER, ASSISTANT
}
