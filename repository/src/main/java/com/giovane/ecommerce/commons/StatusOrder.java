package com.giovane.ecommerce.commons;

public enum StatusOrder {
    CANCELED, COMPLETED, FAILED, PENDING_PAYMENT, PROCESSING, REFUNDING
}
