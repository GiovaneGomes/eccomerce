package com.giovane.ecommerce.repository;

import com.giovane.ecommerce.entity.Order;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OrderRepository extends MongoRepository<Order, String> {
}
