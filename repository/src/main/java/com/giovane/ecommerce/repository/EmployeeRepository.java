package com.giovane.ecommerce.repository;

import com.giovane.ecommerce.entity.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EmployeeRepository extends MongoRepository<Employee, String> {
}
