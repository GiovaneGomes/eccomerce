package com.giovane.ecommerce.repository;

import com.giovane.ecommerce.entity.Product;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepository extends MongoRepository<Product, String> {
}
