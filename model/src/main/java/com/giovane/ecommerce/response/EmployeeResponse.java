package com.giovane.ecommerce.response;

import com.giovane.ecommerce.commons.Department;
import com.giovane.ecommerce.entity.ZipCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class EmployeeResponse {
    private String id;
    private String name;
    private String image;
    private BigDecimal salary;
    private String login;
    private String password;
    private String phoneNumber;
    private LocalDate beginContract;
    private Department department;
    private ZipCode zipCode;
}
