package com.giovane.ecommerce.response;

import com.giovane.ecommerce.entity.ZipCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class CustomerResponse {
    private String id;
    private String name;
    private String image;
    private String login;
    private String password;
    private ZipCode zipCode;
}
