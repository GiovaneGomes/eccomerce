package com.giovane.ecommerce.response;

import com.giovane.ecommerce.commons.StatusOrder;
import com.giovane.ecommerce.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class OrderResponse {
    private String id;
    private LocalDate dataCreated;
    private List<Product> products;
    private StatusOrder status;
}
