package com.giovane.ecommerce.request;

import com.giovane.ecommerce.entity.ZipCode;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CustomerRequest {
    @NotBlank
    private String name;
    @NotBlank
    private String image;
    @NotBlank
    private String login;
    @NotBlank
    private String password;
    @NotNull
    private ZipCode zipCode;
}
