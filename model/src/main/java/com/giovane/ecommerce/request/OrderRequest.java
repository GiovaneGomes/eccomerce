package com.giovane.ecommerce.request;

import com.giovane.ecommerce.commons.StatusOrder;
import com.giovane.ecommerce.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class OrderRequest {
    private String id;
    @NotNull
    private LocalDate dataCreated;
    @NotNull
    private List<Product> products;
    @NotNull
    private StatusOrder status;
}
