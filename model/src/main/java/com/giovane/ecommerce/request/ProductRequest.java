package com.giovane.ecommerce.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ProductRequest {
    private String id;
    @NotBlank
    private String name;
    @NotNull
    private BigDecimal price;
    @NotNull
    private Double weight;
    @NotBlank
    private String image;
}
