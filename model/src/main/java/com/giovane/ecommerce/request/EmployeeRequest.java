package com.giovane.ecommerce.request;

import com.giovane.ecommerce.commons.Department;
import com.giovane.ecommerce.entity.ZipCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class EmployeeRequest {
    private String id;
    @NotBlank
    private String name;
    @NotBlank
    private String image;
    @NotNull
    private BigDecimal salary;
    @NotBlank
    private String login;
    @NotBlank
    private String password;
    @NotBlank
    private String phoneNumber;
    @NotNull
    private LocalDate beginContract;
    @NotNull
    private Department department;
    @NotNull
    private ZipCode zipCode;
}
