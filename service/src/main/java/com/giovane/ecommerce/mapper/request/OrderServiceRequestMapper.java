package com.giovane.ecommerce.mapper.request;

import com.giovane.ecommerce.entity.Order;
import com.giovane.ecommerce.request.OrderRequest;

public class OrderServiceRequestMapper {

    public static Order toEntityOrder(OrderRequest orderRequest) {
        return Order.builder()
                .id(orderRequest.getId())
                .dataCreated(orderRequest.getDataCreated())
                .products(orderRequest.getProducts())
                .status(orderRequest.getStatus())
                .build();
    }

}
