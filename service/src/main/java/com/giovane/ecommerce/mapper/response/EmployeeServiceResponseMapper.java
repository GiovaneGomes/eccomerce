package com.giovane.ecommerce.mapper.response;

import com.giovane.ecommerce.entity.Employee;
import com.giovane.ecommerce.response.EmployeeResponse;

public class EmployeeServiceResponseMapper {

    public static EmployeeResponse toEmployeeServiceResponse(Employee employee) {
        return EmployeeResponse.builder()
                .id(employee.getId())
                .name(employee.getName())
                .image(employee.getImage())
                .salary(employee.getSalary())
                .login(employee.getLogin())
                .password(employee.getPassword())
                .phoneNumber(employee.getPhoneNumber())
                .beginContract(employee.getBeginContract())
                .department(employee.getDepartment())
                .zipCode(employee.getZipCode())
                .build();

    }

}
