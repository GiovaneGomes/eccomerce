package com.giovane.ecommerce.mapper.request;

import com.giovane.ecommerce.entity.Product;
import com.giovane.ecommerce.request.ProductRequest;

public class ProductServiceRequestMapper {

    public static Product toEntityProduct(ProductRequest productRequest) {
        return Product.builder()
                .id(productRequest.getId())
                .name(productRequest.getName())
                .price(productRequest.getPrice())
                .weight(productRequest.getWeight())
                .image(productRequest.getImage())
                .build();
    }

}
