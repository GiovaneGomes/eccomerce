package com.giovane.ecommerce.mapper.response;

import com.giovane.ecommerce.entity.Order;
import com.giovane.ecommerce.response.OrderResponse;

public class OrderServiceResponseMapper {

    public static OrderResponse toOrderServiceResponse(Order order) {
        return OrderResponse.builder()
                .id(order.getId())
                .dataCreated(order.getDataCreated())
                .products(order.getProducts())
                .status(order.getStatus())
                .build();
    }

}
