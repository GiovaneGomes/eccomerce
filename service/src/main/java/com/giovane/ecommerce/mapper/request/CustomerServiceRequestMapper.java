package com.giovane.ecommerce.mapper.request;

import com.giovane.ecommerce.entity.Customer;
import com.giovane.ecommerce.request.CustomerRequest;
import lombok.experimental.UtilityClass;

@UtilityClass
public class CustomerServiceRequestMapper {

    public static Customer toEntityCustomer(CustomerRequest custumerRequest) {
        return Customer.builder()
                .id(custumerRequest.getId())
                .name(custumerRequest.getName())
                .image(custumerRequest.getImage())
                .login(custumerRequest.getLogin())
                .password(custumerRequest.getPassword())
                .zipCode(custumerRequest.getZipCode())
                .build();
    }

}
