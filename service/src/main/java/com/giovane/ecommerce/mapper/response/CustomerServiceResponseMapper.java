package com.giovane.ecommerce.mapper.response;

import com.giovane.ecommerce.entity.Customer;
import com.giovane.ecommerce.response.CustomerResponse;
import lombok.experimental.UtilityClass;

@UtilityClass
public class CustomerServiceResponseMapper {

    public static CustomerResponse toCustomerServiceResponse(Customer custumer) {
        return CustomerResponse.builder()
                .id(custumer.getId())
                .name(custumer.getName())
                .image(custumer.getImage())
                .login(custumer.getLogin())
                .password(custumer.getPassword())
                .zipCode(custumer.getZipCode())
                .build();
    }

}
