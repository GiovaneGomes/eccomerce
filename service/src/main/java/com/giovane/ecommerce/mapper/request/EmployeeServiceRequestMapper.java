package com.giovane.ecommerce.mapper.request;

import com.giovane.ecommerce.entity.Employee;
import com.giovane.ecommerce.request.EmployeeRequest;

public class EmployeeServiceRequestMapper {

    public static Employee toEntityEmployee(EmployeeRequest employeRequest) {
        return Employee.builder()
                .id(employeRequest.getId())
                .name(employeRequest.getName())
                .image(employeRequest.getImage())
                .salary(employeRequest.getSalary())
                .login(employeRequest.getLogin())
                .password(employeRequest.getPassword())
                .phoneNumber(employeRequest.getPhoneNumber())
                .beginContract(employeRequest.getBeginContract())
                .department(employeRequest.getDepartment())
                .zipCode(employeRequest.getZipCode())
                .build();

    }

}
