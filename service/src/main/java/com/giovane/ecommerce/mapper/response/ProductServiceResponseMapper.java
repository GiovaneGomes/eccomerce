package com.giovane.ecommerce.mapper.response;

import com.giovane.ecommerce.entity.Product;
import com.giovane.ecommerce.response.ProductResponse;

public class ProductServiceResponseMapper {

    public static ProductResponse toProductServiceResponse(Product product) {
        return ProductResponse.builder()
                .id(product.getId())
                .name(product.getName())
                .price(product.getPrice())
                .weight(product.getWeight())
                .image(product.getImage())
                .build();
    }

}
