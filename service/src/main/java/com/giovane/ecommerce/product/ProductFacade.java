package com.giovane.ecommerce.product;

import com.giovane.ecommerce.entity.Product;
import com.giovane.ecommerce.mapper.response.ProductServiceResponseMapper;
import com.giovane.ecommerce.request.ProductRequest;
import com.giovane.ecommerce.response.ProductResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import java.util.List;
import static com.giovane.ecommerce.mapper.request.ProductServiceRequestMapper.toEntityProduct;
import static com.giovane.ecommerce.mapper.response.ProductServiceResponseMapper.toProductServiceResponse;

@AllArgsConstructor
@Component
public class ProductFacade {

    private final ProductService service;

    public ProductResponse save(ProductRequest productRequest) {
        return toProductServiceResponse(service.save(toEntityProduct(productRequest)));
    }

    public ProductResponse update(ProductRequest productRequest, String id) {
        Product product = service.findById(id);
        productRequest.setId(product.getId());
        return toProductServiceResponse(service.save(toEntityProduct(productRequest)));
    }

    public void deleteById(String id) {
        service.deleteById(service.findById(id).getId());
    }

    public ProductResponse findById(String id) {
        return toProductServiceResponse(service.findById(id));
    }

    public List<ProductResponse> findAll() {
        return service.findAll().stream()
                .map(ProductServiceResponseMapper::toProductServiceResponse)
                .toList();
    }

}
