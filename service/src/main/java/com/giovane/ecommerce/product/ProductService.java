package com.giovane.ecommerce.product;

import com.giovane.ecommerce.entity.Product;
import com.giovane.ecommerce.exceptions.notfound.NotFoundException;
import com.giovane.ecommerce.repository.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;

@AllArgsConstructor
@Service
public class ProductService {

    private final ProductRepository repository;

    public Product save(Product product) {
        return repository.save(product);
    }

    public void deleteById(String id) {
        repository.deleteById(id);
    }

    public Product findById(String id) {
        return repository.findById(id)
                .orElseThrow(() -> new NotFoundException("ID not found"));
    }

    public List<Product> findAll() {
        return repository.findAll();
    }

}
