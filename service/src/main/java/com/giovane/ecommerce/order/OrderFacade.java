package com.giovane.ecommerce.order;

import com.giovane.ecommerce.entity.Order;
import com.giovane.ecommerce.mapper.response.OrderServiceResponseMapper;
import com.giovane.ecommerce.request.OrderRequest;
import com.giovane.ecommerce.response.OrderResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import java.util.List;
import static com.giovane.ecommerce.mapper.request.OrderServiceRequestMapper.toEntityOrder;
import static com.giovane.ecommerce.mapper.response.OrderServiceResponseMapper.toOrderServiceResponse;

@AllArgsConstructor
@Component
public class OrderFacade {

    private final OrderService service;

    public OrderResponse save(OrderRequest orderRequest) {
        return toOrderServiceResponse(service.save(toEntityOrder(orderRequest)));
    }

    public OrderResponse update(OrderRequest orderRequest, String id) {
        Order order = service.findById(id);
        orderRequest.setId(order.getId());
        return toOrderServiceResponse(service.save(toEntityOrder(orderRequest)));
    }

    public void deleteById(String id) {
        service.deleteById(service.findById(id).getId());
    }

    public OrderResponse findById(String id) {
        return toOrderServiceResponse(service.findById(id));
    }

    public List<OrderResponse> findAll() {
        return service.findAll().stream()
                .map(OrderServiceResponseMapper::toOrderServiceResponse)
                .toList();
    }

}
