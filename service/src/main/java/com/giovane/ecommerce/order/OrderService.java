package com.giovane.ecommerce.order;

import com.giovane.ecommerce.entity.Order;
import com.giovane.ecommerce.exceptions.notfound.NotFoundException;
import com.giovane.ecommerce.repository.OrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;

@AllArgsConstructor
@Service
public class OrderService {

    private final OrderRepository repository;

    public Order save(Order order) {
        return repository.save(order);
    }

    public void deleteById(String id) {
        repository.deleteById(id);
    }

    public Order findById(String id) {
        return repository.findById(id)
                .orElseThrow(() -> new NotFoundException("ID not found"));
    }

    public List<Order> findAll() {
        return repository.findAll();
    }

}
