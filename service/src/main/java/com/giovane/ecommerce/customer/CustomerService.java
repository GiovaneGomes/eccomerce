package com.giovane.ecommerce.customer;

import com.giovane.ecommerce.entity.Customer;
import com.giovane.ecommerce.exceptions.notfound.NotFoundException;
import com.giovane.ecommerce.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;

@AllArgsConstructor
@Service
public class CustomerService {

    private final CustomerRepository repository;

    public Customer save(Customer customer) {
        return repository.save(customer);
    }

    public void deleteById(String id) {
        repository.deleteById(id);
    }

    public Customer findById(String id) {
        return repository.findById(id)
                .orElseThrow(() -> new NotFoundException("ID not found"));
    }

    public List<Customer> findAll() {
        return repository.findAll();
    }

}
