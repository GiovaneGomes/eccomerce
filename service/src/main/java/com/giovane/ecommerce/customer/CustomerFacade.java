package com.giovane.ecommerce.customer;

import com.giovane.ecommerce.entity.Customer;
import com.giovane.ecommerce.mapper.response.CustomerServiceResponseMapper;
import com.giovane.ecommerce.request.CustomerRequest;
import com.giovane.ecommerce.response.CustomerResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import java.util.List;
import static com.giovane.ecommerce.mapper.request.CustomerServiceRequestMapper.toEntityCustomer;
import static com.giovane.ecommerce.mapper.response.CustomerServiceResponseMapper.toCustomerServiceResponse;

@AllArgsConstructor
@Component
public class CustomerFacade {

    private final CustomerService service;

    public CustomerResponse save(CustomerRequest customerRequest) {
        return toCustomerServiceResponse(service.save(toEntityCustomer(customerRequest)));
    }

    public CustomerResponse update(CustomerRequest customerRequest, String id) {
        Customer byId = service.findById(id);
        customerRequest.setId(byId.getId());
        return toCustomerServiceResponse(service.save(toEntityCustomer(customerRequest)));
    }

    public void deleteById(String id) {
        service.deleteById(service.findById(id).getId());
    }

    public CustomerResponse findById(String id) {
        return toCustomerServiceResponse(service.findById(id));
    }

    public List<CustomerResponse> findAll() {
        return service.findAll().stream()
                .map(CustomerServiceResponseMapper::toCustomerServiceResponse)
                .toList();
    }

}
