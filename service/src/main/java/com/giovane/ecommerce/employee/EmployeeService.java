package com.giovane.ecommerce.employee;

import com.giovane.ecommerce.entity.Employee;
import com.giovane.ecommerce.exceptions.notfound.NotFoundException;
import com.giovane.ecommerce.repository.EmployeeRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;

@AllArgsConstructor
@Service
public class EmployeeService {

    private final EmployeeRepository repository;

    public Employee save(Employee employee) {
        return repository.save(employee);
    }

    public void deleteById(String id) {
        repository.deleteById(id);
    }

    public Employee findById(String id) {
        return repository.findById(id)
                .orElseThrow(() -> new NotFoundException("ID not found"));
    }

    public List<Employee> findAll() {
        return repository.findAll();
    }

}
