package com.giovane.ecommerce.employee;

import com.giovane.ecommerce.entity.Employee;
import com.giovane.ecommerce.mapper.response.EmployeeServiceResponseMapper;
import com.giovane.ecommerce.request.EmployeeRequest;
import com.giovane.ecommerce.response.EmployeeResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import java.util.List;
import static com.giovane.ecommerce.mapper.request.EmployeeServiceRequestMapper.toEntityEmployee;
import static com.giovane.ecommerce.mapper.response.EmployeeServiceResponseMapper.toEmployeeServiceResponse;

@AllArgsConstructor
@Component
public class EmployeeFacade {

    private final EmployeeService service;

    public EmployeeResponse save(EmployeeRequest employeeRequest) {
        return toEmployeeServiceResponse(service.save(toEntityEmployee(employeeRequest)));
    }

    public EmployeeResponse update(EmployeeRequest employeeRequest, String id) {
        Employee employee = service.findById(id);
        employeeRequest.setId(employee.getId());
        return toEmployeeServiceResponse(service.save(toEntityEmployee(employeeRequest)));
    }

    public void deleteById(String id) {
        service.deleteById(service.findById(id).getId());
    }

    public EmployeeResponse findById(String id) {
        return toEmployeeServiceResponse(service.findById(id));
    }

    public List<EmployeeResponse> findAll() {
        return service.findAll().stream()
                .map(EmployeeServiceResponseMapper::toEmployeeServiceResponse)
                .toList();
    }

}
